import $ from 'jquery'

export default function(url, data, f) {
  $.ajax({
    type: 'post',
    url: 'http://39.97.241.19:8080/' + url,
    // url: 'http://localhost:8080/' + url,
    contentType: 'application/json; charset=utf-8',
    data: JSON.stringify(data),
    // dataType : "json",
    success: f,
    error: function(message) {
      console.log(message)
    }
  })
}

